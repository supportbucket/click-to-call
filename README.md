# README # 


# Contact Sky  for Reporting Sky Talk Service Problems #

Sky Talk customer services can provide assistance with any number of problems related to home phone service. Examples of common issues include no dial tone, beeping on the line, noise or line interference, and phone line faults. Call Sky on 0843 504 7160 or freephone 0800 151 2747 for urgent customer care over the phone between the hours of 8:30am and 11:30pm, Monday to Sunday (UK time).  By way of Sky Talk contact, problems can be detected and a proper solution can be provided. Business customers may also phone the Sky Talk contact number shown to report service problems. 

## Sky Talk help agent will assist with scheduling an appointment. ##

If the problem is area-wide, an agent will let you know this. In most cases, problems can be solved without the need of a home repair visit. If on-location repair is necessary, a Sky’s customer service team operated its telephone helpline between the hours of 8:30am and 11:30pm (UK time), 7 days a week. For after-hours service, you can still access FAQs and raise a support ticket online at www.sky.com.

“Pay As You Talk” is [yet another option to consider](http://www.faqtory.co/sky/). This may actually be the best option for those who typically do not make many calls from home. Charges are only incurred when calls are made. This plan is automatically provided to those who subscribe to Sky broadband service, but have opted not to pay for monthly phone service. Those who already have an active phone line can request to only pay a line rental fee and keep their current telephone number. Contact Sky Talk if you wish to make use of this option for your home phone services.